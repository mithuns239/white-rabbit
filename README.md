# node-js-sample

A Node.js app using [Express, MongoDB].

## Running Locally

Make sure you have [Node.js] and the [MongoDB] installed.

# MongoDB Username and Password not required

Create db as 'rabbit' and create a collection named 'users' inside it

```sh
git clone https://gitlab.com/mithuns239/white-rabbit.git
cd white-rabbit
npm install
nodemon index.js
```

App should now be running on [localhost:3000](http://localhost:3000/).
